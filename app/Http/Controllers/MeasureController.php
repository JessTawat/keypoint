<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Khsing\World\World;
use Khsing\World\Models\Continent;
use App\Measure;
use App\Asset;
use App\Measuretype;
use App\Measurestatus;
use DataTables;
use Redirect;
use Alert;


class MeasureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('measure.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        $regions = World::Continents();
        $currencies = World::Countries()->unique();
        $assets = Asset::all();
        $measuretypes = Measuretype::all();
        $measurestatus = Measurestatus::all();

        return view('measure.create')
        ->with([
            'regions'=>$regions,
            'assets' => $assets,
            'currencies' => $currencies,
            'measuretypes' => $measuretypes,
            'measurestatus' => $measurestatus
        ]); 
    }

    public function world(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');

        $continent = Continent::getByCode($value);
        $countries = $continent->children();

        $output = '<option value=""></option>';
         foreach($countries as $row)
         {
          $output .= '<option value="'.$row->name.'">'.$row->name.'</option>';
         }
        echo $output;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $measure = new Measure;

        $this->validate($request,[
            'measurename' => 'required',
            'assetclass' => 'required',
            'measuretype' => 'required'
        ]);

        $measure->measure_name = $request->input('measurename');
        $measure->measure_symbol = $request->input('measuresymbol');
        $measure->asset_class = $request->input('assetclass');
        $measure->measure_type = $request->input('measuretype');
        $measure->measure_status = $request->input('measurestatus');
        $measure->region = $request->input('region');
        $measure->country = $request->input('country');
        $measure->currency = $request->input('currency');
        $measure->capitalization = $request->input('capitalization');
        $measure->frequency = $request->input('frequency');
        $measure->description = $request->input('description');
        $measure->measure_source = $request->input('measuresource');
        $measure->measure_source_reference = $request->input('measuresourcereference');
        $measure->base_date = \Carbon\Carbon::parse($request->input('basedate'));
        $measure->incept_date = \Carbon\Carbon::parse($request->input('inceptdate'));
        $measure->discontinue_date = \Carbon\Carbon::parse($request->input('discontinuedate'));
        $measure->notes = $request->input('notes');
        $measure->user_id = auth()->user()->id;

        if($measure->save()){
            Alert::success('Saved!');
        }

        return Redirect('/measures');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regions = World::Continents();
        $currencies = World::Countries()->unique();
        $assets = Asset::all();
        $measuretypes = Measuretype::all();
        $measurestatus = Measurestatus::all();

        $measure = Measure::find($id);

        return view('measure.edit')
        ->with([
            'measure' => $measure,
            'regions'=>$regions,
            'assets' => $assets,
            'currencies' => $currencies,
            'measuretypes' => $measuretypes,
            'measurestatus' => $measurestatus
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $measure = Measure::find($id);

        $this->validate($request,[
            'measurename' => 'required',
            'assetclass' => 'required',
            'measuretype' => 'required'
        ]);

        $measure->measure_name = $request->input('measurename');
        $measure->measure_symbol = $request->input('measuresymbol');
        $measure->asset_class = $request->input('assetclass');
        $measure->measure_type = $request->input('measuretype');
        $measure->measure_status = $request->input('measurestatus');
        $measure->region = $request->input('region');
        $measure->country = $request->input('country');
        $measure->currency = $request->input('currency');
        $measure->capitalization = $request->input('capitalization');
        $measure->frequency = $request->input('frequency');
        $measure->description = $request->input('description');
        $measure->measure_source = $request->input('measuresource');
        $measure->measure_source_reference = $request->input('measuresourcereference');
        $measure->base_date = \Carbon\Carbon::parse($request->input('basedate'));
        $measure->incept_date = \Carbon\Carbon::parse($request->input('inceptdate'));
        $measure->discontinue_date = \Carbon\Carbon::parse($request->input('discontinuedate'));
        $measure->notes = $request->input('notes');
        $measure->user_id = auth()->user()->id;
        
        if($measure->save()){
            Alert::success('Updated!');
        }

        return Redirect('/measures');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function country() 
    {  
        return view('measure.country');
    }

    function getdata()
    {
        $measure = Measure::all();
        return Datatables::of($measure)
        ->AddColumn('action', function($measure){
            return '<a href="measures/' . $measure->id . '/edit" class= "edit"><button class="wave-effect btn btn-success btn-bordred wave-light"><i class="fa fa-edit"></i></button></a>
                    <a href="#" id="'. $measure->id .'" class="delete"><button class="wave-effect btn btn-danger btn-bordred wave-light delete"><i class="fa fa-times"></i></button></a>';
        })
        ->make(true);
    }

    function removedata(Request $request)
    {
        $measure = Measure::find($request->input('id'));
        $measure->delete();
    }
}
