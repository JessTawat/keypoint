<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager;
use DataTables;
use Redirect;
use Alert;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manager.index');
    }

    function managerdata()
    {
        $managers = Manager::all();
        return Datatables::of($managers)
        ->AddColumn('action', function($managers){
            return '<a href="managers/' . $managers->id . '/edit" class= "edit"><button class="wave-effect btn btn-success btn-bordred wave-light"><i class="fa fa-edit"></i></button></a>
                    <a href="#" id="'. $managers->id .'" class="delete"><button class="wave-effect btn btn-danger btn-bordred wave-light delete"><i class="fa fa-times"></i></button></a>';
        })
        ->make(true);
    }

    function removedata(Request $request)
    {
        $manager = Manager::find($request->input('id'));
        $manager->delete();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manager.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'managername' => 'required'
        ]);

        $manager = new Manager;
        $manager->manager_name = $request->input('managername');
        $manager->manager_status_id = $request->input('managerstatus');
        $manager->manager_product_id = $request->input('managerproduct');
        $manager->manager_industry_id = $request->input('managerindustry');
        $manager->qualification_id = $request->input('qualification');
        $manager->company_info = $request->input('companyinfo');
        $manager->manager_evaluation = $request->input('managerevaluation');
        $manager->address = $request->input('address');
        $manager->city = $request->input('city');
        $manager->province_id = $request->input('state');
        $manager->country_id = $request->input('country');
        $manager->phone = $request->input('phone');
        $manager->other_office = $request->input('otheroffice');
        $manager->year_founded = $request->input('yearfounded');
        $manager->discountinue_date = $request->input('discontinuedate');
        $manager->engagement_minimum = $request->input('engagementminimum');
        $manager->engagement_notes = $request->input('engagementnotes');
        $manager->institutional_minimum = $request->input('institutionalminimum');
        $manager->institutional_fees = $request->input('institutionalfee');
        $manager->private_client_minimum = $request->input('privateclientminimum');
        $manager->private_client_fees = $request->input('privateclientfee');
        $manager->aboriginal_minimum = $request->input('aboriginalminimum');
        $manager->aboriginal_fees = $request->input('aborignalfee');
        $manager->aboriginal_notes = $request->input('aboriginalnotes');
        $manager->npf_minimum = $request->input('npfminimum');
        $manager->npf_fees = $request->input('npffee');
        $manager->affiliations = $request->input('affiliation');
        $manager->notes = $request->input('notes');
        if ($manager->save()){
            Alert::success('Saved!');
        };

        return Redirect('/managers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manager = Manager::find($id);
        return view('manager.edit')->with('manager', $manager);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'managername' => 'required'
        ]);

        
        $manager = Manager::find($id);
        $manager->manager_name = $request->input('managername');
        
        if($manager->save()){
            Alert::success('Updated!');
        }

        return Redirect('/managers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
