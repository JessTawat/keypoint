<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

use Redirect;
use DataTables;
use Validator;
use Alert;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $client = new Client;

        // $client = $request->validate([
        //     'firstname' => 'required|unique',
        //     'lastname' => 'required',
        // ]);

        $client->client_id = $request->input('client_id');
        $client->first_name = $request->input('firstname');
        $client->middle_name = $request->input('middlename');
        $client->last_name = $request->input('lastname');
        $client->save();

        return view('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);

        return view('client.edit')->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $client = Client::find($id);

        $client = $request->validate([
            'firstname' => 'required|unique',
            'lastname' => 'required',
        ]);
        
        $client->first_name = $request->input('firstname');
        $client->middle_name = $request->input('middlename');
        $client->last_name = $request->input('lastname');
        $client->save();

        return Redirect('/clients');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findorFail($id);
        $client->delete();

        return Redirect('/clients');

    }

    function datatable(){

        $clients = Client::all();
        return Datatables::of($clients)
        ->AddColumn('action', function($client){
            return '<a href="clients/' . $client->client_id . '/edit" class= "btn btn-success"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        })
        ->make(true);
    }
    
    function removedata(Request $request)
    {
        $client = Client::find($id);
        $client->delete();
    }

}
