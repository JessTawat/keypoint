<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\Measuretype;
use App\Measurestatus;
use DataTables;
use Redirect;
use Alert;

class AssetsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('asset.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->get('action_measuretype') == 'insertmeasuretype'){

            // $this->validate($request,[
            //     'measuretype' => 'required',
            //     'notes' => 'required'
            // ]);

            $measuretype = new Measuretype;
            $measuretype->measure_type = $request->input('measuretype');
            $measuretype->notes = $request->input('notes');
            if($measuretype->save()){
                Alert::success('Saved!');
            }
        }

        if($request->get('action_measuretype') == 'updatemeasuretype'){

            $this->validate($request,[
                'measuretype' => 'required',
                'notes' => 'required'
            ]);

            $measuretype = Measuretype::find($request->get('measuretype_id'));
            $measuretype->measure_type = $request->input('measuretype');
            $measuretype->notes = $request->input('notes');
            if($measuretype->save()){
                Alert::success('Updated!');
            }
        }

        if($request->get('button_action') == 'insert'){
            // $this->validate($request,[
            //     'assetclass' => 'required',
            //     'depth' => 'required|size:10|numeric'
            // ]);

            $asset = new Asset;
            $asset->asset_class = $request->input('assetclass');
            $asset->depth = $request->input('depth');
            if($asset->save()){
                Alert::success('Saved!');
            }
        }

        if($request->get('button_action') == 'update'){

            // $this->validate($request,[
            //     'assetclass' => 'required',
            //     'depth' => 'required|size:10|numeric'
            // ]);

            $asset = Asset::find($request->get('asset_id'));
            $asset->asset_class = $request->input('assetclass');
            $asset->depth = $request->input('depth');
            if($asset->save()){
                Alert::success('Updated!');
            }
        }

        return Redirect('/dim');
    }

    public function storemeasurestatus(Request $request)
    {
        if($request->get('button_action') == 'insert')
        {
            $measurestatus = new Measurestatus;
            $measurestatus->measure_status = $request->input('measurestatus');
            $measurestatus->save();
            Alert::success('Saved!');
        }

        if($request->get('button_action') == 'update')
        {
            $measurestatus = Measurestatus::find($request->get('measurestatus_id'));
            $measurestatus->measure_status = $request->input('measurestatus');
            $measurestatus->save();
            Alert::success('Updated!');
        }
        
        return Redirect('/dim');
    }


    function fetchasset(Request $request)
    {
        $id = $request->input('id');
        $asset = Asset::find($id);
        $output = array(
            'asset_class'    =>  $asset->asset_class,
            'depth'     =>  $asset->depth
        );

        echo json_encode($output);
    }

    function fetchmeasuretype(Request $request)
    {
        $id = $request->input('id');
        $measuretype = Measuretype::find($id);
        $output = array(
        'measure_type'    =>  $measuretype->measure_type,
        'notes'     =>  $measuretype->notes
        );

        echo json_encode($output);
    }

    function fetchmeasurestatus(Request $request)
    {
        $id = $request->input('id');
        $measurestatus = Measurestatus::find($id);
        $output = array(
        'measure_status'    =>  $measurestatus->measure_status
        );

        echo json_encode($output);
    }

    function getdata()
    {
        $dim = Asset::all();
        return Datatables::of($dim)
        ->AddColumn('action', function($dim){
            return '<a href="#" id="'. $dim->id .'" class= "edit"><button class="wave-effect btn btn-success btn-bordred wave-light"><i class="fa fa-edit"></i></button></a>
                    <a href="#" id="'. $dim->id .'" class="delete"><button class="wave-effect btn btn-danger btn-bordred wave-light delete"><i class="fa fa-times"></i></button></a>';
        })
        ->make(true);
    }

    function measuretypedata()
    {
        $measuretype = Measuretype::all();
        return Datatables::of($measuretype)
        ->AddColumn('action', function($measuretype){
            return '<a href="#" id="'. $measuretype->id .'" class= "editmeasuretype"><button class="wave-effect btn btn-success btn-bordred wave-light"><i class="fa fa-edit"></i></button></a>
                    <a href="#" id="'. $measuretype->id .'" class="removemeasuretype"><button class="wave-effect btn btn-danger btn-bordred wave-light delete"><i class="fa fa-times"></i></button></a>';
        })
        ->make(true);
    }

    function measurestatus()
    {
        $measurestatus = Measurestatus::all();
        return Datatables::of($measurestatus)
        ->AddColumn('action', function($measurestatus){
            return '<a href="#" id="'. $measurestatus->id .'" class= "editmeasurestatus"><button class="wave-effect btn btn-success btn-bordred wave-light"><i class="fa fa-edit"></i></button></a>
                    <a href="#" id="'. $measurestatus->id .'" class="removemeasurestatus"><button class="wave-effect btn btn-danger btn-bordred wave-light delete"><i class="fa fa-times"></i></button></a>';
        })
        ->make(true);
    }

    function removeasset(Request $request)
    {
        $asset = Asset::find($request->input('id'));
        $asset->delete();
    }

    function removemeasuretype(Request $request)
    {
        $measuretype = Measuretype::find($request->input('id'));
        $measuretype->delete();
    }

    function removemeasurestatus(Request $request)
    {
        $measurestatus = Measurestatus::find($request->input('id'));
        $measurestatus->delete();
    }

}
