<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measuretype extends Model
{
    protected $table = 'market_measuretype';
}
