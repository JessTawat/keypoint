<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();
Route::get('/', 'PageController@index');

// Dim
Route::resource('dim', 'AssetsController');
// asset
Route::get('dimdata', 'AssetsController@getdata')->name('dim.data');
Route::get('removeasset', 'AssetsController@removeasset')->name('remove.asset');
Route::get('fetchasset', 'AssetsController@fetchasset')->name('fetch.asset');
// Measure Type
Route::get('measuretypedata', 'AssetsController@measuretypedata')->name('measuretype.data');
// Route::post('storemeasuretype', 'AssetsController@storemeasuretype')->name('store.measuretype');
Route::get('removemeasuretype', 'AssetsController@removemeasuretype')->name('remove.measuretype');
Route::get('fetchmeasuretype', 'AssetsController@fetchmeasuretype')->name('fetch.measuretype');

// Measure Status
Route::get('measurestatusdata', 'AssetsController@measurestatus')->name('measurestatus.data');
Route::post('storemeasurestatus', 'AssetsController@storemeasurestatus')->name('store.measurestatus');
Route::get('removemeasurestatus', 'AssetsController@removemeasurestatus')->name('remove.measurestatus');
Route::get('fetchmeasurestatus', 'AssetsController@fetchmeasurestatus')->name('fetch.measurestatus');

// client
Route::resource('clients', 'ClientController');
Route::get('datatable', 'ClientController@getdata')->name('clients.data');

// measures
Route::resource('measures', 'MeasureController');
Route::get('getdata', 'MeasureController@getdata')->name('measures.data');
Route::put('world', 'MeasureController@world')->name('measures.world');
Route::get('removedata', 'MeasureController@removedata')->name('measures.removedata');

//Managers
Route::resource('managers', 'ManagerController');
Route::get('managerdata', 'ManagerController@managerdata')->name('managers.data');
Route::get('removemanager', 'ManagerController@removedata')->name('manager.removedata');





