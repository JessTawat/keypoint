<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Keypoint

# Installation #

- run composer install
- npm install
- npm run dev
- php artisan migrate
- php artisan db:seed

# Packages uses: #

## Datatables
<p>composer require yajra/laravel-datatables-oracle:"~8.0"</p>

**Register provider and facade on config/app.php**

'providers' => [
    ...,
    Yajra\DataTables\DataTablesServiceProvider::class,
]

'aliases' => [
    ...,
    'DataTables' => Yajra\DataTables\Facades\DataTables::class,
]

**Publish Vendor**

$ php artisan vendor:publish --provider="Yajra\DataTables\DataTablesServiceProvider"


## Sweet-Alert
<p>composer require yajra/laravel-datatables-oracle:"~8.0"</p>




