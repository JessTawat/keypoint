@extends('layouts.app')

@section('content')
<!-- begin panel -->
<div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
    <!-- begin panel-heading -->
    <div class="panel-heading p-0">
        <div class="panel-heading-btn m-r-10 m-t-10">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        </div>
        <!-- begin nav-tabs -->
        <div class="tab-overflow">
            <ul class="nav nav-tabs nav-tabs-inverse">
                <li class="nav-item prev-button"><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
                <li class="nav-item"><a href="#nav-tab-1" data-toggle="tab" class="nav-link active"><i class="fa fa-cogs"></i> <span class="d-none d-md-inline">Asset</span></a>
                <li class="nav-item"><a href="#nav-tab-2" data-toggle="tab" class="nav-link"><i class="fa fa-sitemap"></i> <span class="d-none d-md-inline">Measure Type</span></a></li>
                <li class="nav-item"><a href="#nav-tab-3" data-toggle="tab" class="nav-link"><i class="fa fa-calendar"></i> <span class="d-none d-md-inline">Measure Status</span></a></li>
            </ul>
        </div>
        <!-- end nav-tabs -->
    </div>
    <!-- end panel-heading -->

    {{-- Asset --}}
    <!-- begin tab-content -->
    <div class="tab-content">
        <!-- begin tab-pane -->
        <div class="tab-pane fade active show" id="nav-tab-1">
            <div class="m-10"><a href="#" id="add_asset" class="btn btn-info btn-sm">New</a></div>
            <div class="table-responsive">
                <table id="measure_asset" class="table table-striped m-b-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Asset Class</th>
                            <th>Depth</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                </table> 
            </div>
            {{-- Asset Modal --}}
            <div id="assetModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" id="asset_form" class="asset_form">
                            <div class="modal-header">
                               <h4 class="modal-title">Add</h4>
                            </div>
                            <div class="modal-body">
                                @csrf
                                <span id="form_output"></span>
                                <div class="form-group">
                                    <label>Asset Class</label>
                                    <input type="text" name="assetclass" id="assetclass" class="form-control{{ $errors->has('assetclass') }}" required autofocus/>

                                    @if ($errors->has('assetclass'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('assetclass') }}</strong>
                                        </span>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label>Depth</label>
                                    <input type="text" name="depth" id="depth" class="form-control{{ $errors->has('depth') }}" required autofocus/>

                                    @if ($errors->has('depth'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('depth') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>
                            <div class="modal-footer">
                                 <input type="hidden" name="asset_id" id="asset_id" value="" />
                                <input type="hidden" name="button_action" id="button_action" value="insert" />
                                <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- end tab-pane -->

        {{-- Measure Type --}}
        <!-- begin tab-pane -->
        <div class="tab-pane fade" id="nav-tab-2">
            <div class="m-10"><a href="#" id="add_measuretype" class="btn btn-info btn-sm">New</a></div>
            <div class="table-responsive">
                <table id="measure_type" class="table table-striped m-b-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Measure Type</th>
                            <th>Notes</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

            {{-- Measure Type Modal --}}
            <div id="measuretypeModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form method="post" id="measuretype_form" class="measuretype_form">
                            <div class="modal-header">
                               <h4 class="modal-title">Add</h4>
                            </div>
                            <div class="modal-body">
                                @csrf
                                <span id="form_output"></span>
                                <div class="form-group">
                                    <label>Measure Type</label>
                                    <input type="text" name="measuretype" id="measuretype" class="form-control{{ $errors->has('measuretype') }}" required autofocus/>

                                    @if ($errors->has('measuretype'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('measuretype') }}</strong>
                                        </span>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea type="text" name="notes" id="notes" class="form-control{{ $errors->has('notes') }}" required autofocus></textarea>

                                    @if ($errors->has('notes'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('notes') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>
                            <div class="modal-footer">
                                 <input type="hidden" name="measuretype_id" id="measuretype_id" value="" />
                                <input type="hidden" name="action_measuretype" id="action_measuretype" value="insertmeasuretype" />
                                <input type="submit" name="submit" id="actiontype" value="Add" class="btn btn-info" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- end tab-pane -->

        {{-- Measure Status --}}
        <!-- begin tab-pane -->
        <div class="tab-pane fade" id="nav-tab-3">
            <div class="m-10"><a href="#" id="add_measurestatus" class="btn btn-info btn-sm">New</a></div>
            <div class="table-responsive">
                <table id="measure_status" class="table table-striped m-b-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Measure status</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

         {{-- Measure Status Modal --}}
         <div id="measurestatusModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="measurestatus_form">
                        <div class="modal-header">
                           <h4 class="modal-title">Add</h4>
                        </div>
                        <div class="modal-body">
                            @csrf
                            <span id="form_output"></span>
                            <div class="form-group">
                                <label>Measure Status</label>
                                <input type="text" name="measurestatus" id="measurestatus" class="form-control{{ $errors->has('measurestatus') }}" required autofocus/>

                                @if ($errors->has('measurestatus'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('measurestatus') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="measurestatus_id" id="measurestatus_id" value="" />
                           <input type="hidden" name="button_action" id="button_action" value="insert" />
                           <input type="submit" name="submit" id="action_measuretype" value="Add" class="btn btn-info" />
                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end tab-pane -->
    </div>
    <!-- end tab-content -->
</div>
<!-- end panel -->
                          
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#measure_asset').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('dim.data') }}",
        "columns":[
            { "data": "id" },
            { "data": "asset_class" },
            { "data": "depth" },
            { "data": "action", orderable:false, searchable: false}
        ]
    });

    $('#add_asset').click(function(){
        $('#assetModal').modal('show');
        $('#asset_form')[0].reset();
        $('#form_output').html('');
        $('#button_action').val('insert');
        $('#action').val('Add');
        $('.modal-title').text('Add Asset');
    });

     $('#student_form').on('submit', function(event){
        event.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url:"{{ route('dim.store') }}",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data) {
                $('#form_output').html(data.success);
                $('#asset_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Asset');
                $('#button_action').val('insert');
                $('#measure_asset').DataTable().ajax.reload();
            }
        })
    });

    $(document).on('click', '.edit', function(){
        var id = $(this).attr("id");
        console.log(id)
        $('#form_output').html('');
        $.ajax({
            url:"{{ route('fetch.asset') }}",
            method:'get',
            data:{id:id},
            dataType:'json',
            success:function(data)
            {
                $('#assetModal').modal('show');
                $('#assetclass').val(data.asset_class);
                $('#depth').val(data.depth);
                $('#asset_id').val(id);
                $('#action').val('Edit');
                $('.modal-title').text('Edit Asset');
                $('#button_action').val('update');
            }
        })
    });

    $(document).on('click', '.delete', function(){
            var id = $(this).attr('id');
        swal({
            title: "Are you sure",
            text: "This record will be deleted permanently.",
            icon: "warning",
            buttons: true,
            dangerMode: true
            })
            .then((removedata) => {
            if (removedata) {
                $.ajax({
                url:"{{ route('remove.asset') }}",
                method:"get",
                data:{id:id},
                success:function(data)
                {
                    swal("Deleted!", "", "success");
                    $('#measure_asset').DataTable().ajax.reload();
                }
            })
            }
            else
            {
                return false;
            }
        });
    });

    $('#measure_type').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('measuretype.data') }}",
        "columns":[
            { "data": "id" },
            { "data": "measure_type" },
            { "data": "notes" },
            { "data": "action", orderable:false, searchable: false}
        ]
    });

       $('#add_measuretype').click(function(){
            $('#measuretypeModal').modal('show');
            $('#measuretype_form')[0].reset();
            $('#form_output').html('');
            $('#action_measuretype').val('insertmeasuretype');
            $('#actiontype').val('Add');
            $('.modal-title').text('Add Measure Type');
    });
  
    $(document).on('click', '.editmeasuretype', function(){
        var id = $(this).attr("id");
        console.log(id)
        $('#form_output').html('');
        $.ajax({
            url:"{{ route('fetch.measuretype') }}",
            method:'get',
            data:{id:id},
            dataType:'json',
            success:function(data)
            {
                $('#measuretypeModal').modal('show');
                $('#measuretype').val(data.measure_type);
                $('#notes').val(data.notes);
                $('#measuretype_id').val(id);
                $('#classModal').modal('show');
                $('#actiontype').val('Edit');
                $('.modal-title').text('Edit Measuretype');
                $('#action_measuretype').val('updatemeasuretype');
            }
        })
    });

    $(document).on('click', '.removemeasuretype', function(){
            var id = $(this).attr('id');
            console.log(id)
        swal({
            title: "Are you sure",
            text: "This record will be deleted permanently.",
            icon: "warning",
            buttons: true,
            dangerMode: true
            })
            .then((measuretype) => {
            if (measuretype) {
                $.ajax({
                url:"{{ route('remove.measuretype') }}",
                method:"get",
                data:{id:id},
                success:function(data)
                {
                    swal("Deleted!", "", "success");
                    $('#measure_type').DataTable().ajax.reload();
                }
            })
            }
            else
            {
                return false;
            }
        });
    });


    // Measure Status
    $('#measure_status').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('measurestatus.data') }}",
        "columns":[
            { "data": "id" },
            { "data": "measure_status" },
            { "data": "action", orderable:false, searchable: false}
        ]
    });

     $('#add_measurestatus').click(function(){
        $('#measurestatusModal').modal('show');
        $('#measuretype_form')[0].reset();
        $('#form_output').html('');
        $('#button_action').val('insert');
        $('#action').val('Add');
        $('.modal-title').text('Add Measure Status');
    });

    $(document).on('click', '.editmeasurestatus', function(){
        var id = $(this).attr("id");
        console.log(id)
        $('#form_output').html('');
        $.ajax({
            url:"{{ route('fetch.measurestatus') }}",
            method:'get',
            data:{id:id},
            dataType:'json',
            success:function(data)
            {
                $('#measurestatusModal').modal('show');
                $('#measurestatus').val(data.measure_status);
                $('#measurestatus_id').val(id);
                $('#action_measuretype').val('Edit');
                $('.modal-title').text('Edit Measure Status');
                $('#button_action').val('update');
            }
        })
    });

    $('#measurestatus_form').on('submit', function(event){
        event.preventDefault();
        var form_measurestatus = $(this).serialize();
        $.ajax({
            url:"{{ route('store.measurestatus') }}",
            method:"POST",
            data:form_measurestatus,
            dataType:"json",
            success:function(data) {
                $('#form_output').html(data.success);
                $('#asset_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Asset');
                $('#button_action').val('insert');
                $('#measure_status').DataTable().ajax.reload();
            }
        })
    });

    $(document).on('click', '.removemeasurestatus', function(){
            var id = $(this).attr('id');
            console.log(id)
        swal({
            title: "Are you sure",
            text: "This record will be deleted permanently.",
            icon: "warning",
            buttons: true,
            dangerMode: true
            })
            .then((measurestatus) => {
            if (measurestatus) {
                $.ajax({
                url:"{{ route('remove.measurestatus') }}",
                method:"get",
                data:{id:id},
                success:function(data)
                {
                    swal("Deleted!", "", "success");
                    $('#measure_status').DataTable().ajax.reload();
                }
            })
            }
            else
            {
                return false;
            }
        });
    });

});
</script>
@endpush 
@endsection


