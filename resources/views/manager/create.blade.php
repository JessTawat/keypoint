@extends('layouts.app')

@section('content')
<div class="container">
        <button id="goback" class="btn btn-success pull-right goback">Go Back</button>

        <h1 class="page-header">New Manager</h1>

<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Manager Details</legend>

<form action="{{ route('managers.store') }}" method="POST" class="margin-bottom-0">
    @csrf
    {{-- Manager Name --}}
    <label class="p-2 text-md-right col-form-label">Manager Name<span class="text-danger">*</span></label>
    <div class="col-md-10"> 
        <input id="managername" type="text" class="form-control{{ $errors->has('managername') }}" name="managername" required autofocus>

        @if ($errors->has('managername'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('managername') }}</strong>
            </span>
        @endif
    </div>

    {{-- Manager Status --}}
    <label class="p-2 text-md-right col-form-label">Manager Status</label>
    <div class="col-md-10"> 
        <select id="managerstatus" class="form-control" name="managerstatus">
            <option value="">--Measure Status--</option>
            {{-- @foreach($assets as $asset)
            <option value="{{ $asset->asset_class }}">{{ $asset->asset_class }}</option>
            @endforeach --}}
        </select>
    </div>

    {{-- Manager Product --}}
    <label class="p-2 text-md-right col-form-label">Manager Product</label>
    <div class="col-md-10"> 
        <select id="managerproduct" class="form-control" name="managerproduct">
            <option value="">--Manager Product--</option>
            {{-- @foreach($assets as $asset)
            <option value="{{ $asset->asset_class }}">{{ $asset->asset_class }}</option>
            @endforeach --}}
        </select>
    </div>
    {{-- Manager Industry --}}
    <label class="p-2 text-md-right col-form-label">Manager Industry</label>
    <div class="col-md-10"> 
        <select id="managerindustry" class="form-control" name="managerindustry">
            <option value="">--Manager Industry--</option>
            {{-- @foreach($assets as $asset)
            <option value="{{ $asset->asset_class }}">{{ $asset->asset_class }}</option>
            @endforeach --}}
        </select>
    </div>

    {{-- Qualification --}}
    <label class="p-2 text-md-right col-form-label">Qualification</label>
    <div class="col-md-10"> 
        <select id="qualification" class="form-control" name="qualification">
            <option value="">--Qualification--</option>
            {{-- @foreach($assets as $asset)
            <option value="{{ $asset->asset_class }}">{{ $asset->asset_class }}</option>
            @endforeach --}}
        </select>
    </div>

    {{-- Company Info --}}
    <label class="p-2 text-md-right col-form-label">Company Info</label>
    <div class="col-md-10"> 
        <textarea id="companyinfo" class="form-control" name="companyinfo"></textarea>
    </div>

    {{-- Managers Info --}}
    <label class="p-2 text-md-right col-form-label">Manager's Evaluation</label>
    <div class="col-md-10"> 
        <textarea id="managerevaluation" class="form-control" name="managerevaluation"></textarea>
    </div>

    {{-- Address --}}
    <label class="p-2 text-md-right col-form-label">Street Address</label>
    <div class="col-md-10"> 
        <input id="address" class="form-control" name="address" />
    </div>
    {{-- City and Postal Code --}}
   <div class="form-group m-t-15">
        <div class="col-md-10">
            <div class="row row-space-6">
                <div class="col-6"> 
                    <label class="p-2 text-md-right col-form-label">City</label>
                    <input id="city" class="form-control" name="city" />
                </div>
                <div class="col-3">
                    <label class="p-2 text-md-right col-form-label">Postal Code</label>
                    <input id="postalcode" class="form-control" name="postalcode" />
                </div>
            </div>
        </div>
    </div>
{{-- State and Contry --}}
<div class="form-group m-t-15">
    <div class="col-md-10">
        <div class="row row-space-6">
            <div class="col-6"> 
                <label class="p-2 text-md-right col-form-label">State</label>
                <select id="state" class="form-control" name="state">
                    <option value="">--State--</option>
                </select>
            </div>
            <div class="col-6">
                <label class="p-2 text-md-right col-form-label">Country</label>
                <select id="country" class="form-control" name="country">
                    <option value="">--Country--</option>
                </select>
            </div>
        </div>
    </div>
</div>

{{-- Phone --}}
<label class="p-2 text-md-right col-form-label">Phone Number</label>
<div class="col-md-5"> 
    <input id="phone" class="form-control" name="phone" />
</div>

{{-- Other Office --}}
<label class="p-2 text-md-right col-form-label">Other Office</label>
<div class="col-md-10"> 
    <input id="otheroffice" class="form-control" name="otheroffice" />
</div>

{{-- Year Founded --}}
<label class="p-2 text-md-right col-form-label">Year Founded</label>
<div class="col-md-5"> 
    <input id="yearfounded" class="form-control" name="yearfounded" />
</div>

{{-- Discountinue Date --}}
<label class="p-2 text-md-right col-form-label">Discountinue Date</label>
<div class="col-md-5"> 
    <input id="discontinuedate" class="form-control" name="discontinuedate" />
</div>

<div class="form-group m-t-15">
    <div class="col-md-10">
        <div class="row row-space-6">
            <div class="col-6">
                {{-- Engagement --}}
                <div id="accordion" class="card-accordion">
                    <!-- begin card -->
                    <div class="card">
                        <div class="card-header bg-blue text-white pointer-cursor" data-toggle="collapse" data-target="#collapseOne">
                            <i class="fas fa-lg fa-fw m-r-10 fa-link"></i>Engagement
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <label>Minimum</label> 
                                    <input id="engagementminimum" class="form-control" name="engagementminimum" />
                                <label class="p-2 text-md-right col-form-label">Notes</label>
                                    <textarea id="engagementnotes" class="form-control" name="engagementnotes"></textarea>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            {{-- Institutional --}}
            <div class="col-6">
                <div id="institutional" class="card-accordion">
                    <!-- begin card -->
                    <div class="card">
                        <div class="card-header bg-blue text-white pointer-cursor" data-toggle="collapse" data-target="#collapseInstitutional">
                            <i class="fas fa-lg fa-fw m-r-10 fa-users"></i>Institutional
                        </div>
                        <div id="collapseInstitutional" class="collapse" data-parent="#institutional">
                            <div class="card-body">
                                <label>Minimum</label> 
                                    <input id="institutionalminimum" class="form-control" name="institutionalminimum" />
                                <label class="p-2 text-md-right col-form-label">Fee</label>
                                    <textarea id="institutionalfee" class="form-control" name="institutionalfee"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group m-t-15">
        <div class="col-md-10">
            <div class="row row-space-6">
            <div class="col-6">
            {{-- Private Client --}}
            <div id="privateclient" class="card-accordion">
                    <!-- begin card -->
                    <div class="card">
                        <div class="card-header bg-blue text-white pointer-cursor" data-toggle="collapse" data-target="#collapsePrivateclient">
                            <i class="fas fa-lg fa-fw m-r-10 fa-lock"></i>Private Client
                        </div>
                        <div id="collapsePrivateclient" class="collapse" data-parent="#privateclient">
                            <div class="card-body">
                                <label>Minimum</label> 
                                    <input id="privateclientminimum" class="form-control" name="privateclientminimum" />
                                <label class="p-2 text-md-right col-form-label">Fee</label>
                                    <textarea id="privateclientfee" class="form-control" name="privateclientfee"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                {{-- Aborignal --}}
                <div id="aboriginal" class="card-accordion">
                        <!-- begin card -->
                        <div class="card">
                            <div class="card-header bg-blue text-white pointer-cursor" data-toggle="collapse" data-target="#collapseAboriginal">
                                <i class="fas fa-lg fa-fw m-r-10 fa-industry"></i>Aboriginal
                            </div>
                            <div id="collapseAboriginal" class="collapse" data-parent="#aboriginal">
                                <div class="card-body">
                                    <label>Minimum</label> 
                                        <input id="aboriginalminimum" class="form-control" name="aboriginalminimum" />
                                    <label>Fee</label>
                                        <textarea id="aborignalfee" class="form-control" name="aborignalfee"></textarea>
                                    <label>Notes</label>
                                        <textarea id="aboriginalnotes" class="form-control" name="aboriginalnotes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="form-group m-t-15">
    <div class="col-md-10">
        <div class="row row-space-6">
            <div class="col-6">
            {{-- NPF --}}
            <div id="npf" class="card-accordion">
                    <!-- begin card -->
                    <div class="card">
                        <div class="card-header bg-blue text-white pointer-cursor" data-toggle="collapse" data-target="#collapsenpf">
                            <i class="fas fa-lg fa-fw m-r-10 fa-wrench"></i>NPF
                        </div>
                        <div id="collapsenpf" class="collapse" data-parent="#npf">
                            <div class="card-body">
                                <label>Minimum</label> 
                                    <input id="npfminimum" class="form-control" name="npfminimum" />
                                <label class="p-2 text-md-right col-form-label">Fee</label>
                                    <textarea id="npffee" class="form-control" name="npffee"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Affiliation --}}
<label class="p-2 text-md-right col-form-label">Affiliation</label>
<div class="col-md-10"> 
    <input id="affiliation" class="form-control" name="affiliation" />
</div>

{{-- Notes --}}
<label class="p-2 text-md-right col-form-label">Notes</label>
<div class="col-md-10"> 
    <input id="notes" class="form-control" name="notes" />
</div>

    <div class="m-3">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('.goback').click(function (){
    swal({
        title: "You have unsaved changes.",
        text: "Are you sure to leave this page and discard your changes?",
        icon: "warning",
        buttons: true,
        })
        .then((goback) => {
        if (goback) {
            window.location.href = "/managers"
        }
        });
    });
});
</script>
@endpush
@endsection


