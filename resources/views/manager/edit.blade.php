@extends('layouts.app')

@section('content')
<div class="container">
        <button id="goback" class="btn btn-success pull-right goback">Go Back</button>

        <h1 class="page-header">New Manager</h1>

<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Manager Details</legend>
<form action="{{route('managers.update', $manager->id )}}" method="POST" class="margin-bottom-0">
    @csrf
    {{-- Measure Name --}}
    <label class="p-2 text-md-right col-form-label">Manager Name<span class="text-danger">*</span></label>
    <div class="col-md-10"> 
        <input value="{{$manager->manager_name}}" type="text" class="form-control{{ $errors->has('managername') }}" name="managername" required autofocus>

        @if ($errors->has('managername'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('managername') }}</strong>
            </span>
        @endif
    </div>

    <div class="m-3">
        {{ method_field('PUT') }}
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('.goback').click(function (){
    swal({
        title: "You have unsaved changes.",
        text: "Are you sure to leave this page and discard your changes?",
        icon: "warning",
        buttons: true,
        })
        .then((goback) => {
        if (goback) {
            window.location.href = "/managers"
        }
        });
    });
});
</script>
@endpush
@endsection


