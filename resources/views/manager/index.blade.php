@extends('layouts.app')

@section('content')
<div class="container">
    <div class="m-10"><a href="{{ route('managers.create')}}" class="btn btn-info pull-right">New</a></div>
    <h1 class="page-header">Manager</h1>
    <div class="table-responsive">
    <table id="manager_table" class="table table-striped m-b-0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th width="15%">Action</th>
                </tr>
            </thead>
        </table> 
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#manager_table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('managers.data') }}",
            "columns":[
                { "data": "id" },
                { "data": "manager_name" },
                { "data": "action", orderable:false, searchable: false}
            ]
        });

       $(document).on('click', '.delete', function(){
            var id = $(this).attr('id');
        swal({
            title: "Are you sure",
            text: "This record will be deleted permanently.",
            icon: "warning",
            buttons: true,
            dangerMode: true
            })
            .then((removedata) => {
            if (removedata) {
                $.ajax({
                url:"{{route('manager.removedata')}}",
                method:"get",
                data:{id:id},
                success:function(data)
                {
                    swal("Deleted!", "", "success");
                    $('#manager_table').DataTable().ajax.reload();
                }
            })
            }
            else
            {
                return false;
            }
        });
    });
});
</script>
@endpush
@endsection


