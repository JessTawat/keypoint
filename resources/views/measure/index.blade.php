@extends('layouts.app')

@section('content')
<div class="container">
    <div class="m-10"><a href="{{ route('measures.create')}}" class="btn btn-info pull-right">New</a></div>
    <h1 class="page-header">Measures</h1>
    <div class="table-responsive">
    <table id="measure_table" class="table table-striped m-b-0">
            <thead>
                <tr>
                    <th>Measure ID</th>
                    <th>Measure Name</th>
                    <th>Asset Class</th>
                    <th>Measure Status</th>
                    <th>Measure Type</th>
                    <th width="15%">Action</th>
                </tr>
            </thead>
        </table> 
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#measure_table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('measures.data') }}",
            "columns":[
                { "data": "id" },
                { "data": "measure_name" },
                { "data": "asset_class" },
                { "data": "measure_status" },
                { "data": "measure_type" },
                { "data": "action", orderable:false, searchable: false}
            ]
        });

       $(document).on('click', '.delete', function(){
            var id = $(this).attr('id');
        swal({
            title: "Are you sure",
            text: "This record will be deleted permanently.",
            icon: "warning",
            buttons: true,
            dangerMode: true
            })
            .then((removedata) => {
            if (removedata) {
                $.ajax({
                url:"{{route('measures.removedata')}}",
                method:"get",
                data:{id:id},
                success:function(data)
                {
                    swal("Deleted!", "", "success");
                    $('#measure_table').DataTable().ajax.reload();
                }
            })
            }
            else
            {
                return false;
            }
        });
    });
});
</script>
@endpush
@endsection


