@extends('layouts.app')

@section('content')
<div class="container">
        <button id="goback" class="btn btn-success pull-right goback">Go Back</button>

        <h1 class="page-header">Edit Measure</h1>

<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Measure Details</legend>

<form action="{{route('measures.update', $measure->id )}}" method="POST" class="margin-bottom-0">
    @csrf
    {{-- Measure Name --}}
    <label class="p-2 text-md-right col-form-label">Measure Name<span class="text-danger">*</span></label>
    <div class="col-md-10"> 
    <input value="{{$measure->measure_name}}" id="measurename" type="text" class="form-control{{ $errors->has('measurename') }}" name="measurename" required autofocus>

        @if ($errors->has('measurename'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('measurename') }}</strong>
            </span>
        @endif
    </div>

    {{-- Measure Symbol --}}
    <label class="p-2 text-md-right col-form-label">Measure Symbol</label>
    <div class="col-md-10"> 
    <input value="{{ $measure->measure_symbol }}" id="measuresymbol" type="text" class="form-control" name="measuresymbol">
    </div>

    <div class="col-md-10"> 
        <div class="row row-space-12">
            <div class="col-6">
                {{-- Asset --}}
                <label class="p-2 text-md-right col-form-label">Asset Class<span class="text-danger">*</span></label>
                <select id="assetclass" class="form-control{{ $errors->has('assetclass') }}" name="assetclass" required autofocus>
                    <option value="{{  $measure->asset_class }}">{{  $measure->asset_class }}</option>
                        @foreach($assets as $asset)
                        <option value="{{ $asset->asset_class }}">{{ $asset->asset_class }}</option>
                        @endforeach
                    </select>
            
                    @if ($errors->has('assetclass'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('assetclass') }}</strong>
                        </span>
                    @endif
            </div>   
            <div class="col-6">
                {{-- Focus --}}
                <label class="p-2 text-md-right col-form-label">Focus</label>
                <select id="focus" class="form-control{{ $errors->has('assetclass') }}" name="focus">
                    <option value=""></option>
                </select>
            </div>   
        </div>
    </div>

    <div class="col-md-10">
        <div class="row row-space-12">
            {{-- Measure Type --}}
            <div class="col-6">
                <label class="p-2 text-md-right col-form-label">Measure Type<span class="text-danger">*</span></label>
                <select name="measuretype" class="form-control{{ $errors->has('measuretype') }}" id="measuretype" required autofocus>
                    <option value="{{$measure->measure_type}}">{{$measure->measure_type}}</option>
                    @foreach ($measuretypes as $measuretype)
                    <option value="{{ $measuretype->measure_type }}">{{ $measuretype->measure_type }}</option>
                    @endforeach
                </select>
        
                @if ($errors->has('measuretype'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('measuretype') }}</strong>
                </span>
            @endif
            </div>
        
            {{-- Measure Status --}}
            <div class="col-6">
                    <label class="p-2 text-md-right col-form-label">Measure Status<span class="text-danger">*</span></label>
                    <select name="measurestatus" class="form-control{{ $errors->has('measurestatus') }}" id="measurestatus" required autofocus>
                        <option value="{{$measure->measure_status}}">{{$measure->measure_status}}</option>
                        @foreach ($measurestatus as $status)
                        <option value="{{ $status->measure_status }}">{{ $status->measure_status }}</option>
                        @endforeach
                    </select>
            
                    @if ($errors->has('measurestatus'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('measurestatus') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    {{-- Region, Country, Curreny --}}
    <div class="col-md-5">
        <label class="p-2 text-md-right col-form-label">Region</label>
        <select name="region" class="form-control dynamic" id="region" data-dependent="country" required autofocus>
            <option value="{{ $measure->region}}">{{ $measure->region}}</option>
            @foreach ($regions as $region)
            <option value="{{$region->name}}" >{{ $region->name }}</option>
            @endforeach
        </select>

        <label class="p-2 text-md-right col-form-label">Country</label>
        <select id="country" name="country" class="form-control">
            <option value="{{ $measure->country }}">{{ $measure->country }}</option>
        </select>

        <label class="p-2 text-md-right col-form-label">Currency</label>
        <select name="currency" class="form-control">
            <option value="{{ $measure->currency }}">{{ $measure->currency }}</option>
            @foreach ($currencies as $currency)
            <option value="{{ $currency->currency_code }}">{{ $currency->currency_code }} - {{ $currency->currency_name }}</option>
            @endforeach
        </select>
    </div>

    {{-- Capitalization --}}
    <div class="col-md-10">
        <label class="p-2 text-md-right col-form-label">Capilization</label>
        <select name="capitalization" class="form-control" id="capitalization">
        <option value="{{ $measure->capitalization }}">{{ $measure->capitalization }}</option>
        </select>
    </div>

     {{-- Frequency --}}
     <div class="col-md-10">
            <label class="p-2 text-md-right col-form-label">Frequency</label>
                <select name="frequency" class="form-control" id="frequency">
                <option value="{{ $measure->frequency }}">{{ $measure->frequency }}</option>                    <option value=""></option>
                </select>
        </div>
    
        {{-- Description --}}
        <div class="col-md-10">
            <label class="p-2 text-md-right col-form-label">Description</label>
               <textarea value="{{ $measure->description }}" id="description" type="text" class="form-control" name="description"></textarea>
        </div>

    {{-- Measure Source --}}
    <div class="col-md-10">
            <label class="p-2 text-md-right col-form-label">Measure Source</label>
            <input value="{{ $measure->measure_source }}" name ="measuresource" class="form-control">
        </div>
    
        {{-- Measure Source Reference --}}
        <div class="col-md-10">
            <label class="p-2 text-md-right col-form-label">Measure Source Reference</label>
               <input value="{{ $measure->measure_source_reference }}" name="measuresourcereference" class="form-control">
        </div>
    
        {{-- Dates --}}
    <div class="form-group m-t-15">
        <div class="col-md-10">
            <div class="row row-space-6">
                <div class="col-4"> 
                    <label class="control-label" for="date">Base Date</label>
                    <div class="input-group">
                    <input value="{{ $measure->base_date }}" class="date form-control" type="text" name="basedate">
                        <div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                    </div>
                </div>
                <div class="col-4">
                    <label class="control-label" for="date">Incept Date</label>
                    <div class="input-group">
                        <input value="{{ $measure->incept_date }}" class="date form-control" type="text" name="inceptdate">
                        <div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                    </div>
                </div>
                <div class="col-4">
                    <label class="control-label" for="date">Discountinue Date</label>
                    <div class="input-group">
                        <input value="{{ $measure->discontinue_date }}" class="date form-control" type="text" name="discontinuedate">
                        <div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
         {{-- Notes --}}
         <div class="col-md-10">
            <label class="p-2 text-md-right col-form-label">Notes</label>
               <textarea value="{{ $measure->notes }}" id="notes" type="text" class="form-control" name="notes"></textarea>
        </div>


    <div class="m-3">
        {{ method_field('PUT') }} 
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('.date').datepicker({  
        format: 'mm-dd-yyyy'
    }); 

    $('.goback').click(function (){
    swal({
        title: "You have unsaved changes.",
        text: "Are you sure to leave this page and discard your changes?",
        icon: "warning",
        buttons: true,
        })
        .then((goback) => {
        if (goback) {
            window.location.href = "/measures"
        }
        });
    });

    $('.dynamic').change(function(){
        if($(this).val() != '')
        {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('measures.world') }}",
                method:"PUT",
                data:{
                    select:select, 
                    value:value, 
                    _token:_token, 
                    dependent:dependent
                },
                success:function(result)
                {
                    $('#'+dependent).html(result);
                }
            })
        }
    });
});
</script>
@endpush
@endsection


