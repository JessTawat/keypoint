@extends('layouts.app')

@section('content')
<div class="container">
<h1 class="page-header">Edit Client</h1>

<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal Information</legend>

{!! Form::open(['action' => ['ClientController@update', $client->client_id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-group row">
    {{ Form::label('First Name','', ['class' => 'p-2 text-md-right col-form-label']) }}
    <div class="col-md-6"> 
    {{ Form::text('firstname', $client->first_name, ['class' => 'form-control', 'placeholder' => 'John'])}}
    </div>
</div>

<div class="form-group row">
        {{ Form::label('Middle Name','', ['class' => 'p-2 text-md-right col-form-label']) }}
        <div class="col-md-6"> 
        {{ Form::text('middlename', $client->middle_name, ['class' => 'form-control', 'placeholder' => 'Smith'])}}
        </div>
</div>

<div class="form-group row">
        {{ Form::label('Last Name','', ['class' => 'p-2 text-md-right col-form-label']) }}
        <div class="col-md-6"> 
        {{ Form::text('lastname', $client->last_name, ['class' => 'form-control', 'placeholder' => 'Doe'])}}
        </div>
</div>

{{ Form::hidden('_method', 'PUT') }}
{{Form::Submit('Update', ['class'=> 'btn btn-primary'])}}

{!!Form::hidden('_method', 'DELETE')!!}
{!!Form::Submit('Delete',['class' => 'btn btn-danger'])!!}


{!! Form::close() !!}

@endsection
