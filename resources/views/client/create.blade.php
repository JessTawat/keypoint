@extends('layouts.app')

@section('content')
<div class="container">
<h1 class="page-header">New Client</h1>

<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Personal Information</legend>

{!! Form::open(['action' => 'ClientController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

<div class="form-group row">
    {{ Form::label('First Name','', ['class' => 'p-2 text-md-right col-form-label']) }}
    <div class="col-md-6"> 
    {{ Form::text('firstname', '', ['class' => 'form-control', 'placeholder' => 'John'])}}
    </div>
</div>

<div class="form-group row">
        {{ Form::label('Middle Name','', ['class' => 'p-2 text-md-right col-form-label']) }}
        <div class="col-md-6"> 
        {{ Form::text('middlename', '', ['class' => 'form-control', 'placeholder' => 'Smith'])}}
        </div>
</div>

<div class="form-group row">
        {{ Form::label('Last Name','', ['class' => 'p-2 text-md-right col-form-label']) }}
        <div class="col-md-6"> 
        {{ Form::text('lastname', '', ['class' => 'form-control', 'placeholder' => 'Doe'])}}
        </div>
</div>

{{Form::Submit('Submit', ['class'=> 'btn btn-primary'])}}

{!! Form::close() !!}

@endsection
