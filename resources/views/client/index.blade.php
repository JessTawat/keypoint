@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="page-header">Client's List</h1>
    <div class="m-10"><a href="/clients/create" id="add_client" class="btn btn-success btn-sm">New</a></div>
    
    <div class="table-responsive">
    <table id="client_table" class="table table-striped m-b-0">
            <thead>
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th width="20%">Action</th>
                </tr>
            </thead>
        </table> 
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        App.init();
        $('#client_table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('clients.data') }}",
            "columns":[
                { "data": "first_name" },
                { "data": "last_name" },
                { "data": "action", orderable:false, searchable: false}
            ]
        });
    });
</script>
@endpush

@endsection


