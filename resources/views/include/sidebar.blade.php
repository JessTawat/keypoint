<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
        <!-- begin sidebar scrollbar -->
        <div data-scrollbar="true" data-height="100%">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <a href="javascript:;" data-toggle="nav-profile">
                        <div class="cover with-shadow"></div>
                        <div class="image">
                            <img src="img/user/user-13.jpg" alt="" />
                        </div>
                        <div class="info">
                            <b class="caret pull-right"></b>
                            {{ Auth::user()->name }}
                        </div>
                    </a>
                </li>
                <li>
                    <ul class="nav nav-profile">
                        <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
                    </ul>
                </li>
            </ul>
            <!-- end sidebar user -->
            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">Navigation</li>

                <li>
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-users"></i>
                        <span>Client</span>
                    </a>
                    {{-- <ul class="sub-menu">
                        <li class="active"><a href="#">Clients</a></li>
                        <li><a href="#">Client's Account</a></li>
                        <li><a href="#">Client's Report</a></li>
                    </ul> --}}
                </li>

                <li>
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-folder-open"></i> 
                        <span>Accounts</span>
                    </a>
                </li>

                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-industry"></i> 
                        <span>Managers</span>
                    </a>
                    <ul class="sub-menu">
                        <li class="active"><a href="{{ route('managers.index') }}">List<i class="fa fa-paper-plane text-theme m-l-5"></i></a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-cubes"></i> 
                        <span>Products</span>
                    </a>
                </li>

                <li class="has-sub">
                    <a href="#">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-flask"></i> 
                        <span>Measures</span> 
                    </a>
                    <ul class="sub-menu">
                        <li class="active"><a href="{{ route('measures.index') }}">List<i class="fa fa-paper-plane text-theme m-l-5"></i></a></li>
                        <li><a href="{{ route('dim.index') }}">Dim</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-unlock-alt"></i> 
                        <span>Securities</span> 
                    </a>
                </li>

                <li>
                    <a href="#">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-address-book"></i> 
                        <span>Contacts</span> 
                    </a>
                </li>

                <li>
                    <a href="#">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-cart-plus"></i> 
                        <span>Items</span> 
                    </a>
                </li>

                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fas fa-lg fa-fw m-r-10 fa-chart-line"></i>
                        <span>Reports</span> 
                    </a>
                    <ul class="sub-menu">
                        <li><a href="javascript:;"><i class="fa fa-paper-plane text-theme m-l-5"></i> General</a></li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret"></b>
                        <i class="fa fa-key"></i>
                        <span>Administration</span>
                    </a>

                    <ul class="sub-menu">
                        <li><a href="#">Login</a></li>
                    </ul>
                </li>
                <!-- begin sidebar minify button -->
				<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
				<!-- end sidebar minify button -->
            </ul>
        </div>      
    </div>

