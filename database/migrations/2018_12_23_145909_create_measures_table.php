<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('measure_name');
            $table->string('measure_symbol');
            $table->string('asset_class');
            $table->string('currency');
            $table->string('country');
            $table->string('capitalization');
            $table->string('style');
            $table->string('region');
            $table->string('frequency');
            $table->text('description');
            $table->string('measure_type');
            $table->string('measure_source');
            $table->string('measure_source_reference');
            $table->string('measure_status');
            $table->date('base_date');
            $table->date('incept_date');
            $table->date('discontinue_date');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measures');
    }
}
