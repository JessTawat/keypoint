<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('manager_name');
            $table->text('company_info')->nullable();
            $table->text('manager_evaluation')->nullable();
            $table->integer('qualification_id')->nullable();
            $table->integer('manager_product_id')->nullable();
            $table->integer('manager_industry_id')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('postal')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->string('other_office')->nullable();
            $table->year('year_founded')->nullable();
            $table->date('discountinue_date')->nullable();
            $table->decimal('engagement_minimum', 12, 0)->nullable();
            $table->text('engagement_notes')->nullable();
            $table->decimal('institutional_minimum', 10, 0)->nullable();
            $table->text('institutional_fees')->nullable();
            $table->decimal('private_client_minimum', 10, 0)->nullable();
            $table->text('private_client_fees')->nullable();
            $table->decimal('aboriginal_minimum', 10, 0)->nullable();
            $table->text('aboriginal_fees')->nullable();
            $table->text('aboriginal_notes')->nullable();
            $table->decimal('npf_minimum', 10, 0)->nullable();
            $table->text('npf_fees')->nullable();
            $table->text('notes')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managers');
    }
}
