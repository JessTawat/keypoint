<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeasureTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('market_measuretype')->insert(array (
            0 => 
            array (
                'id' => 1,
                'measure_type' => 'Index'
            ),
            1 => 
            array (
                'id' => 2,
                'measure_type' => 'Benchmark'
            ),
            2 => 
            array (
                'id' => 3,
                'measure_type' => 'Rates & Yields'
            ),
            3 => 
            array (
                'id' => 4,
                'measure_type' => 'Currencies'
            ),
            4 => 
            array (
                'id' => 5,
                'measure_type' => 'Commodity'
            ),
            5 => 
            array (
                'id' => 6,
                'measure_type' => 'Economic'
            ),
            6 => 
            array (
                'id' => 7,
                'measure_type' => 'Business Cycle Indicators'
            ),
        ));
    }
}
