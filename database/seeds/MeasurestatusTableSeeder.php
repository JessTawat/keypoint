<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeasurestatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('market_measurestatus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'measure_status' => 'Active'
            ),
            1 => 
            array (
                'id' => 2,
                'measure_status' => 'Discontinued'
            ),
            2 => 
            array (
                'id' => 3,
                'measure_status' => 'To Research'
            ),
            3 => 
            array (
                'id' => 4,
                'measure_status' => 'To Add'
            ),
            4 => 
            array (
                'id' => 5,
                'measure_status' => 'Future Consideration'
            ),
            5 => 
            array (
                'id' => 6,
                'measure_status' => 'Not Considered'
            ),
            6 => 
            array (
                'id' => 7,
                'measure_status' => 'Pending'
            ),
        ));
    }
}
