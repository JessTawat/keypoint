<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AsssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('market_assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'asset_class' => 'Cash & Cash Equivalents',
                'depth' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'asset_class' => 'Cash',
                'depth' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'asset_class' => 'Short-Term Securities',
                'depth' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'asset_class' => 'Money Market',
                'depth' => 2,
            ),
            4 => 
            array (
                'id' => 5,
                'asset_class' => 'Fixed Income',
                'depth' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'asset_class' => 'Universe',
                'depth' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'asset_class' => 'Government Bonds',
                'depth' => 2,
            ),
        ));
    }
}
