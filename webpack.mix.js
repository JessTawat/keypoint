const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

   // Admin JS Template   
mix.copyDirectory('resources/assets/plugins/jquery', 'public/assets/plugins/jquery')
mix.copyDirectory('resources/assets/plugins/jquery-ui', 'public/assets/plugins/jquery-ui')
mix.copyDirectory('resources/assets/plugins/bootstrap/4.1.3/js', 'public/assets/plugins/bootstrap/4.1.3/js')
mix.copyDirectory('resources/assets/plugins/slimscroll', 'public/assets/plugins/slimscroll')
mix.copyDirectory('resources/assets/plugins/js-cookie', 'public/assets/plugins/js-cookie')
mix.copyDirectory('resources/assets/js/theme', 'public/assets/js/theme')
mix.copyDirectory('resources/assets/js', 'public/assets/js')
mix.copyDirectory('resources/assets/plugins/pace', 'public/assets/plugins/pace')

  // Admin CSS Template
mix.copyDirectory('resources/assets/plugins/bootstrap/4.1.3/css', 'public/assets/plugins/bootstrap/4.1.3/css')
mix.copyDirectory('resources/assets/plugins/font-awesome', 'public/assets/plugins/font-awesome')
mix.copyDirectory('resources/assets/plugins/animate', 'public/assets/plugins/animate')
mix.copyDirectory('resources/assets/css/default', 'public/assets/css/default')
mix.copyDirectory('resources/assets/css/default/theme', 'public/assets/css/default/theme')
mix.copyDirectory('resources/assets/plugins/DataTables', 'public/assets/plugins/DataTables')
mix.copyDirectory('resources/assets/plugins/ionicons', 'public/assets/plugins/ionicons')
mix.copyDirectory('resources/assets/img', 'public/img');

